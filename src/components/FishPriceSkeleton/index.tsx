import React from 'react';
import Skeleton from 'react-loading-skeleton';
import styles from './styles.module.scss';

const FishPriceSkeleton = () => {
  return (
    <div className={styles.container}>
      <Skeleton width="100%" height={250} />
    </div>
  );
};

export default React.memo(FishPriceSkeleton);
