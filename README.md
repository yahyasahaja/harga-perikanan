# Harga Perikanan
[![pipeline status](https://gitlab.com/yahyasahaja/harga-perikanan/badges/master/pipeline.svg)](https://gitlab.com/yahyasahaja/harga-perikanan/commits/master)

---

[DEMO](https://hargaperikanan.ngopi.men)
I'm using **React JS** (using typescript) as a framework to develop this app. Implementing SPA client side rendering bootstrapped CRA

## Project Structure

- ``src/components`` contains all shared components
- ``src/store`` contains all stores, could be represented as ViewModel
- ``src/screens`` contains all the screens inside this app, structured by route logic

## Libraries
- For the UI, I'm using `reactstrap`
- Styling using `SCSS`
- State management using `Redux`
- Form bootstrapping using `json-reactform`

## Architecture

I'm using (Model-View-ViewModel) as an app architecture. 
- The `Model` can be found at Stein
- The `View` can be found at `screens`
- The `ViewModel` can be represented as the redux reducers itself. So all the requests can only be triggered from ViewModel. There's no way to direct interaction between Model and View

## PWA
- Implementing PWA to create the app-shell
- Code Splitting
- Manifest
- Caching. This caching method is for options: using `cache-and-network` caching policy. So the data keep updated while still cached.

## CI/CD
Thanks for Gitlab CI and my aws account that this can be up at [https://hargaperikanan.ngopi.men](https://hargaperikanan.ngopi.men)

## TO RUN LOCALLY
- clone repo
- run `yarn install`
- run `yarn start`