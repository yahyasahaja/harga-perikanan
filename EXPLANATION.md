# EXPLANATION

---

## FILTERING
- The resource is just available to filter one of the a column using `search` query parameter. So I made the UI like if one filter selected, the another filters would be cleared.
- For the UX itself actually it's better to be able to filter based on more than one column.
- Because it's well-known that a city is based on a province, so I made the filter for city depending on it's province.
- Firstly, I'm thinking to make it could be searched like if we're using `%SOME_TEXT%` on mysql. But again, the resource hasn't provide that capability.
- So finally, I decided to use 3 filters: by province, city, and size.

## SORTING
- I've read the `Stein` documentation, I couldn't find sorting part there. 
- Actually if using table, I could just made functionality to be able clicking a column to sort it. 
- My UI is not using table, and for PWA itself, table is not a good decision to be shown for mobile view. So there's no sorting here.
- Because I'm using card, that is using inifinite scroll method, it's not a nice decision to sort based on the fetched data (by client side).

## DATA MUTATION
- The requirements written are just adding the data.
- I'm using `json-reactform` to update handle the form bootstraping using initial format.
- But, because it's just for bootstrapping, it can't be full controlled. Firstly, I'm thinking about making the province and city could be like the filter (dependant), however, because after the initial data loaded the options can't be changed, then I made the city just as text type.
- For the city input, I'm using text input type, because after a province changed, the options for the city dropdown can not be changed, and also, there's a "null" value for the resource. To handle that actually I've tried to changing dynamically for city input type. But again, it can't be changed after bootstrapping with `json-reactform`.
- Also, for typescript, that `json-reactform` still hasn't compatible.


## UI LIBRARY
- At the first commit, I've installed all the initial libraries that would be used. But after I know that `json-reactform` using bootstrap, I removed the `material-ui` and install `react-bootstrap`
- But, after take a look at `json-reactform`, it's using `reactstrap`, `react-select`. Ok then, I refactor the previous usage with `reactstrap` 