export default {
  Komoditas: {
    type: 'text',
    required: true,
  },
  Provinsi: {
    type: 'text',
    required: true,
  },
  Kota: {
    type: 'text',
    required: true,
  },
  'Ukuran (Size)': {
    type: 'number',
    required: true,
  },
  Harga: {
    type: 'number',
    required: true,
  },
  Save: {
    // button submit
    type: 'submit',
  },
};
