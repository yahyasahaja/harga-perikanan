import React from 'react';
import styles from './styles.module.scss';

const FloatingActionButton: React.FC<React.ButtonHTMLAttributes<
  HTMLButtonElement
>> = ({ children, ...others }) => {
  return (
    <button className={styles.container} {...others}>
      {children}
    </button>
  );
};

export default FloatingActionButton;
