import React from 'react';
import FishPriceCard from './index';
import { shallow } from 'enzyme';
import { FishPrice } from 'api/fishPricelistAPI';

const fishPrice: FishPrice = {
  area_kota: 'MALANG',
  area_provinsi: 'JAWA TIMUR',
  komoditas: 'Komoditas e',
  price: '20000',
  size: '10',
  tgl_parsed: 'parsed',
  timestamp: '1203535',
  uuid: 'uuidnya',
};

describe('<FishPriceCard/>', () => {
  it('Should be able to render correct content value', () => {
    const wrapper = shallow(<FishPriceCard {...fishPrice} />);

    const provinsi = wrapper.find('[data-testid="card-provinsi"]');
    expect(provinsi.text()).toEqual(fishPrice.area_provinsi);
    const kota = wrapper.find('[data-testid="card-kota"]');
    expect(kota.text()).toEqual(fishPrice.area_kota);
    const komoditas = wrapper.find('[data-testid="card-komoditas"]');
    expect(komoditas.text()).toEqual(fishPrice.komoditas);
    const size = wrapper.find('[data-testid="card-size"]');
    expect(size.text()).toEqual(fishPrice.size);
    const price = wrapper.find('[data-testid="card-price"]');
    expect(price.text()).toEqual(fishPrice.price);
    wrapper.unmount();
  });
});
