import { createSlice, Action, PayloadAction } from '@reduxjs/toolkit';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '..';

export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;

export interface ToastState {
  isShown: boolean;
  message: string;
}

const toastInitialState: ToastState = {
  isShown: false,
  message: '',
};

export const toastSlice = createSlice({
  name: 'toast',
  initialState: toastInitialState,
  reducers: {
    showToast: (state, { payload }: PayloadAction<string>) => {
      state.message = payload;
      state.isShown = true;
    },
    hideToast: (state) => {
      state.isShown = false;
    },
  },
});

export const toastReducer = toastSlice.reducer;

export const { showToast, hideToast } = toastSlice.actions;
