import {
  capitalize,
  convertArrayToLabelValue,
  convertWordToLabelValue,
} from './utils';

describe('Utils', () => {
  it('capitalize()', () => {
    const words = 'harga_perikanan_Indonesia';
    const expectedResult = 'Harga Perikanan Indonesia';
    const result = capitalize(words);
    expect(result).toBe(expectedResult);
  });

  it('convertWordToLabelValue()', () => {
    const words = 'harga_perikanan_Indonesia';
    const expectedResult = {
      label: 'Harga Perikanan Indonesia',
      value: words,
    };
    const result = convertWordToLabelValue(words);
    expect(result.label).toBe(expectedResult.label);
    expect(result.value).toBe(expectedResult.value);
  });

  it('convertArrayToLabelValue()', () => {
    const array = ['satu_dua', 'tiga_empat'];
    const expectedResult = [
      {
        label: 'Satu Dua',
        value: array[0],
      },
      {
        label: 'Tiga Empat',
        value: array[1],
      },
    ];

    const result = convertArrayToLabelValue(array);
    expect(result.length).toBe(expectedResult.length);
    expect(result[0].label).toBe(expectedResult[0].label);
    expect(result[1].label).toBe(expectedResult[1].label);
  });
});
