import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Toast, ToastBody, ToastHeader } from 'reactstrap';

import styles from './App.module.scss';
import './variables.scss';
import { generateAsyncComponent } from './components/AsyncComponent';
import OverlayLoading from './components/OverlayLoading';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from 'store';
import { hideToast } from 'store/Toast';

const Home = generateAsyncComponent(() =>
  import(/* webpackChunkName: "Home" */ './screens/Home')
);

const App = () => {
  const { isShown, message } = useSelector(
    (store: RootState) => store.toastStore
  );

  const dispatch = useDispatch();

  const hideToastCallback = React.useCallback(() => {
    dispatch(hideToast());
  }, [dispatch]);

  return (
    <div className={styles.container}>
      <Switch>
        <Route path="/home" component={Home} />
        <Redirect from="*" to="/home" />
      </Switch>
      <OverlayLoading />
      <div className={styles['toast-container']}>
        <Toast isOpen={isShown}>
          <ToastHeader icon="primary" toggle={hideToastCallback}>
            Notification
          </ToastHeader>
          <ToastBody>{message}</ToastBody>
        </Toast>
      </div>
    </div>
  );
};

export default App;
