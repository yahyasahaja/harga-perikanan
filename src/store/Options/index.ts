import { createSlice, PayloadAction, Action } from '@reduxjs/toolkit';
import {
  AreaOption,
  MappedAreaOption,
  SizeOption,
  fetchAreaOptionApi,
  fetchSizeOptionApi,
  LabelValue,
} from '../../api/fishPricelistAPI';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '..';
import { convertWordToLabelValue } from 'utils';
import { LOCAL_AREA_OPTIONS_URI, LOCAL_SIZE_OPTIONS_URI } from 'config';

export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;

export interface OptionState {
  isFetchingArea: boolean;
  isFetchingSize: boolean;
  areaOptions: AreaOption[];
  mappedAreaOptions: MappedAreaOption[];
  sizeOptions: SizeOption[];
  mappedSizeOptions: LabelValue[];
}

const optionInitialState: OptionState = {
  isFetchingArea: false,
  isFetchingSize: false,
  areaOptions: [],
  mappedAreaOptions: [],
  sizeOptions: [],
  mappedSizeOptions: [],
};

type AreaOptionsMap = {
  [provinceName: string]: LabelValue[];
};

export const optionSlice = createSlice({
  name: 'options',
  initialState: optionInitialState,
  reducers: {
    setIsFetchingArea: (state, { payload }: PayloadAction<boolean>) => {
      state.isFetchingArea = payload;
    },
    setIsFetchingSize: (state, { payload }: PayloadAction<boolean>) => {
      state.isFetchingSize = payload;
    },
    setAreaOption: (
      state,
      { payload: areaOptions }: PayloadAction<AreaOption[]>
    ) => {
      state.areaOptions = areaOptions;

      const areaOptionsMap: AreaOptionsMap = {};
      areaOptions.forEach((areaOption) => {
        if (!areaOptionsMap[areaOption.province]) {
          areaOptionsMap[areaOption.province] = [];
        }
        if (areaOption.city) {
          areaOptionsMap[areaOption.province].push(
            convertWordToLabelValue(areaOption.city)
          );
        }
      });

      const mappedAreaOptions: MappedAreaOption[] = [];
      for (const i in areaOptionsMap) {
        mappedAreaOptions.push({
          province: convertWordToLabelValue(i),
          cities: areaOptionsMap[i],
        });
      }
      state.mappedAreaOptions = mappedAreaOptions;
    },
    setSizeOption: (
      state,
      { payload: sizeOptions }: PayloadAction<SizeOption[]>
    ) => {
      state.sizeOptions = sizeOptions;
      state.mappedSizeOptions = sizeOptions.map((sizeOption) => ({
        label: sizeOption.size,
        value: sizeOption.size,
      }));
    },
  },
});

export const {
  setAreaOption,
  setIsFetchingArea,
  setIsFetchingSize,
  setSizeOption,
} = optionSlice.actions;

export const optionReducer = optionSlice.reducer;

export const getAreaFromLocal = () => {
  try {
    const area = localStorage.getItem(LOCAL_AREA_OPTIONS_URI);
    if (area) return JSON.parse(area);
  } catch (err) {
    console.log(err);
    return;
  }
};

export const setAreaToLocal = (areaOptions: AreaOption[]) => {
  localStorage.setItem(LOCAL_AREA_OPTIONS_URI, JSON.stringify(areaOptions));
};

export const getSizeFromLocal = () => {
  try {
    const size = localStorage.getItem(LOCAL_SIZE_OPTIONS_URI);
    if (size) return JSON.parse(size);
  } catch (err) {
    console.log(err);
    return;
  }
};

export const setSizeToLocal = (sizeOptions: SizeOption[]) => {
  localStorage.setItem(LOCAL_SIZE_OPTIONS_URI, JSON.stringify(sizeOptions));
};

//CACHE AND FETCH POLICY
export const fetchAreaOption = (): AppThunk => async (dispatch) => {
  try {
    const areaFromLocal: AreaOption[] | undefined = getAreaFromLocal();
    if (areaFromLocal) {
      dispatch(setAreaOption(areaFromLocal));
    } else {
      dispatch(setIsFetchingArea(true));
    }

    const { data: areaOptions } = await fetchAreaOptionApi();
    dispatch(setAreaOption(areaOptions));
    setAreaToLocal(areaOptions);
  } catch (err) {
    console.log('', err);
  } finally {
    dispatch(setIsFetchingArea(false));
  }
};

//CACHE AND FETCH POLICY
export const fetchSizeOption = (): AppThunk => async (dispatch) => {
  try {
    const sizeFromLocal: SizeOption[] | undefined = getSizeFromLocal();
    if (sizeFromLocal) {
      dispatch(setSizeOption(sizeFromLocal));
    } else {
      dispatch(setIsFetchingSize(true));
    }
    const { data: sizeOptions } = await fetchSizeOptionApi();
    dispatch(setSizeOption(sizeOptions));
    setSizeToLocal(sizeOptions);
  } catch (err) {
    console.log('', err);
  } finally {
    dispatch(setIsFetchingSize(false));
  }
};
