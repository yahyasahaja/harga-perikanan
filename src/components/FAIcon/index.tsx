import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconPrefix, IconName } from '@fortawesome/fontawesome-svg-core';
import styles from './styles.module.scss';

type Props = {
  name: IconName;
  prefix: IconPrefix;
};

const FAIcon = (props: Props) => {
  const { name, prefix } = props;

  return (
    <div className={styles.container}>
      <FontAwesomeIcon icon={[prefix, name]} />
    </div>
  );
};

export default React.memo(FAIcon);
