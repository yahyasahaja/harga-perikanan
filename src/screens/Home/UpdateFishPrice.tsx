import React from 'react';
import styles from './updateFishPrice.module.scss';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import JsonToForm from 'json-reactform';

import FloatingActionButton from 'components/FloatingActionButton';
import FAIcon from 'components/FAIcon';
import { FishPrice, LabelValue } from 'api/fishPricelistAPI';
import { v1 as uuidv1 } from 'uuid';
import { useDispatch, useSelector } from 'react-redux';
import { addFishPrices } from 'store/FishPrice';
import { RootState } from 'store';

type MappedValues = {
  [key: string]: any;
};

const DEFAULT_FORM_MODEL: any = {
  Komoditas: {
    type: 'text',
    required: true,
  },
  Provinsi: {
    type: 'select',
    required: true,
    options: [],
  },
  Kota: {
    type: 'text',
    required: true,
  },
  'Ukuran (Size)': {
    type: 'select',
    required: true,
    options: [],
  },
  Harga: {
    type: 'number',
    required: true,
  },
  Save: {
    type: 'submit',
  },
};

const UpdateFishPrice: React.FC = () => {
  //STATE
  const dispatch = useDispatch();
  const {
    mappedAreaOptions,
    mappedSizeOptions,
    isFetchingArea,
    isFetchingSize,
  } = useSelector((store: RootState) => store.optionStore);
  const [isUpdateDialogOpened, setIsUpdateDialogOpened] = React.useState(false);
  const [mappedValues, setMappedValues] = React.useState<MappedValues>();
  const [provinceOptions, setProvinceOptions] = React.useState<LabelValue[]>(
    []
  );

  const handleClose = () => {
    setIsUpdateDialogOpened(false);
  };

  const generateOptions = () => {
    const model = DEFAULT_FORM_MODEL;
    model.Provinsi.options = provinceOptions;
    model['Ukuran (Size)'].options = mappedSizeOptions;
    console.log(model);
    return model;
  };

  React.useEffect(() => {
    setProvinceOptions(
      mappedAreaOptions.map((areaOption) => areaOption.province)
    );
  }, [mappedAreaOptions]);

  const handleSubmit = React.useCallback(async () => {
    if (mappedValues) {
      const loc = {
        area_kota: mappedValues['Kota'],
        area_provinsi: mappedValues['Provinsi'].value,
        size: mappedValues['Ukuran (Size)'].value,
        price: mappedValues['Harga'],
        komoditas: mappedValues['Komoditas'],
      };

      const fishPrice: FishPrice = {
        ...loc,
        uuid: uuidv1(),
        tgl_parsed: new Date().toString(),
        timestamp: Date.now().toString(),
      };

      let notBlank = true;
      for (const i in fishPrice) {
        if (!fishPrice[i] || fishPrice[i] === '') notBlank = false;
      }

      if (notBlank) {
        await dispatch(addFishPrices(fishPrice));
        setIsUpdateDialogOpened(false);
      }
    }
  }, [dispatch, mappedValues]);

  return (
    <>
      <FloatingActionButton
        onClick={() => {
          setIsUpdateDialogOpened(true);
        }}
      >
        <FAIcon name="plus" prefix="fas" />{' '}
        <div className={styles['fab-text']}>Tambah Harga</div>
      </FloatingActionButton>
      <Modal isOpen={isUpdateDialogOpened} toggle={handleClose}>
        <ModalHeader toggle={handleClose}>Modal heading</ModalHeader>
        <ModalBody>
          <div className={styles.wrapper}>
            {!isFetchingArea && !isFetchingSize ? (
              <JsonToForm
                model={generateOptions()}
                onChange={({ value }) => {
                  setMappedValues(value);
                }}
                onSubmit={handleSubmit}
              />
            ) : (
              <div>Sedang memuat</div>
            )}
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button color="primary" onClick={handleSubmit}>
            Save Changes
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default UpdateFishPrice;
