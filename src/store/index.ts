import {
  configureStore,
  getDefaultMiddleware,
  combineReducers,
  EnhancedStore,
} from '@reduxjs/toolkit';

import { fishPriceReducer } from './FishPrice';
import { overlayLoadingReducer } from './OverlayLoading';
import { optionReducer } from './Options';
import { toastReducer } from './Toast';

const rootReducer = combineReducers({
  fishPriceStore: fishPriceReducer,
  overlayLoadingStore: overlayLoadingReducer,
  optionStore: optionReducer,
  toastStore: toastReducer,
});
const middleware = getDefaultMiddleware();

export const store = configureStore({
  reducer: rootReducer,
  middleware,
});

export type RootState = ReturnType<typeof rootReducer>;

declare global {
  interface Window {
    clientStore: EnhancedStore;
  }
}

export default window.clientStore = store;
